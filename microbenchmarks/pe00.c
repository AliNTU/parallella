#include <stdint.h>
#include "e_lib.h"

// global memory management -- stack start address, giving 8KB of       stack
asm(".global __stack_start__;");
asm(".set __stack_start__,0x6000;");
asm(".global __heap_start__;");
asm(".set __heap_start__,0x0000;");
asm(".global __heap_end__;");
asm(".set __heap_end__,0x1fff;");

int main()
{
	unsigned* data = (unsigned*)0x3000;
	*data = 10;
	unsigned* result = (unsigned*)0x3400;
	*result = 0;
	unsigned *totaltime;
	unsigned* initTime = (unsigned*)0x2004;
	*initTime = 0;

	e_ctimer_config_t event = E_CTIMER_CLK;

	// Timer Start 
        e_ctimer_set(E_CTIMER_0, E_CTIMER_MAX);
	e_ctimer_start(E_CTIMER_0, event);
        
        unsigned time_e = e_ctimer_get(E_CTIMER_0);

	//write data to ecore(0,1)
	unsigned* var = (unsigned*) e_get_global_address((unsigned)0, (unsigned)1, (unsigned*)0x2000);
	e_dma_copy((unsigned*)var, (unsigned*)data, sizeof(unsigned));

	//wait until ecore(0,1) updates the data
	while(*result == 0)
	{
	}
	 
	// Timer Stop
        unsigned time_s = e_ctimer_get(E_CTIMER_0);
        e_ctimer_stop(E_CTIMER_0);
        unsigned clocks = (time_e - time_s);

	//write timer value at this address
	totaltime = (unsigned *)0x2004;
	*totaltime = clocks;
	
	return 0;
}
