#!/bin/bash

set -e

ESDK=${EPIPHANY_HOME}
ELIBS=${ESDK}/tools/host/lib
EINCS=${ESDK}/tools/host/include
ELDF=${ESDK}/bsps/current/fast.ldf

SCRIPT=$(readlink -f "$0")
EXEPATH=$(dirname "$SCRIPT")
cd $EXEPATH

CROSS_PREFIX=
case $(uname -p) in
	arm*)
		# Use native arm compiler (no cross prefix)
		CROSS_PREFIX=
		;;
	   *)
		# Use cross compiler
		CROSS_PREFIX="arm-linux-gnueabihf-"
		;;
esac

# Build HOST side application
${CROSS_PREFIX}gcc -g src/host.c -o bin/host.elf -I ${EINCS} -L ${ELIBS} -le-hal  #-le-loader

# Build DEVICE side program
e-gcc -T ${ELDF} -g src/pe00.c -o bin/pe00.elf -le-lib 
e-gcc -T ${ELDF} -g src/pe01.c -o bin/pe01.elf -le-lib 
 
# Convert ebinary to SREC file
e-objcopy --srec-forceS3 --output-target srec bin/pe00.elf bin/pe00.srec
e-objcopy --srec-forceS3 --output-target srec bin/pe01.elf bin/pe01.srec
