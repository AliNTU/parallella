#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "e_lib.h"

// global memory management -- stack start address, giving 8KB of       stack
asm(".global __stack_start__;");
asm(".set __stack_start__,0x6000;");
asm(".global __heap_start__;");
asm(".set __heap_start__,0x0000;");
asm(".global __heap_end__;");
asm(".set __heap_end__,0x1fff;");

int main()
{	
	unsigned *temp = (unsigned*)0x2000;
	int i;

        for (i = 0; i < 2000; i++)
	{	
		*temp = *temp + 5;
	}
	
	unsigned* result = (unsigned *) e_get_global_address((unsigned)0, (unsigned)0, (unsigned*)0x3400);
	e_dma_copy((unsigned*)result, (unsigned*)temp, sizeof(unsigned)); 
	
	return 0;
}
