#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <e-hal.h>

int main(void)
{
	unsigned i, j, curr_address;
	e_platform_t platform;
	e_epiphany_t dev;
	e_mem_t emem;

	//initialize device
	e_init(NULL);
	e_reset_system();
	e_get_platform_info(&platform);

	// Open a workgroup
	e_open(&dev, 0, 0, platform.rows, platform.cols);

	// Load PE side
	e_load("pe00.srec", &dev, 0, 0, E_TRUE);
	e_load("pe01.srec", &dev, 0, 1, E_TRUE);

	//sleep until ecores finish exceution
	usleep(2000000);

	// Read Data from the PEs
	unsigned result;
	unsigned time;

	e_read(&dev, 0, 0, 0x3400, &result, sizeof(unsigned));
	printf("From eCore (%d,%d), Result = %d\n", 0, 0, result);
	e_read(&dev, 0, 1, 0x2000, &result, sizeof(unsigned));
	printf("From eCore (%d,%d), Result = %d\n", 0, 1, result);
	e_read(&dev, 0, 0, 0x2004, &time, sizeof(unsigned));
	printf("eCore (%d,%d) ticks value at address 0X2004 = %d\n",0,0,time);
	printf("Core to Core round time = %f msec\n",(((float)time)/600000000)*1000);
	
	// Close the workgroup
	 e_close(&dev);
	
	//Finalize e-platform connection
	 e_finalize();


	return 0;
}
